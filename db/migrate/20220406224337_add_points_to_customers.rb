# frozen_string_literal: true

class AddPointsToCustomers < ActiveRecord::Migration[6.1]
  def change
    add_column :customers, :points, :decimal, default: 0
  end
end
