# frozen_string_literal: true

class CreateInvitations < ActiveRecord::Migration[6.1]
  def change
    create_table :invitations do |t|
      t.references :inviter, null: false, foreign_key: { to_table:  :customers }
      t.references :invited, null: false, foreign_key: { to_table:  :customers }
      t.date :invitation_date
      t.time :invitation_time
      t.datetime :accepted_at
      t.timestamps
    end
    # make composite key
    add_index :invitations, %i[inviter_id invited_id invitation_time invitation_date], name: 'by_date_time_inviter_invited', unique: true
  end
end
