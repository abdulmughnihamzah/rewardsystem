# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_04_06_224337) do

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.decimal "points", default: "0.0"
    t.index ["name"], name: "index_customers_on_name", unique: true
  end

  create_table "invitations", force: :cascade do |t|
    t.integer "inviter_id", null: false
    t.integer "invited_id", null: false
    t.date "invitation_date"
    t.time "invitation_time"
    t.datetime "accepted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["invited_id"], name: "index_invitations_on_invited_id"
    t.index ["inviter_id", "invited_id", "invitation_time", "invitation_date"], name: "by_date_time_inviter_invited", unique: true
    t.index ["inviter_id"], name: "index_invitations_on_inviter_id"
  end

  add_foreign_key "invitations", "customers", column: "invited_id"
  add_foreign_key "invitations", "customers", column: "inviter_id"
end
