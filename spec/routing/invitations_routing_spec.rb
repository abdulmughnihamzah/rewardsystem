# frozen_string_literal: true

require 'rails_helper'
RSpec.describe V1::InvitationsController, type: :routing do
  describe 'routing' do
    it 'routes to #upload' do
      expect(post: '/v1/invitations/upload').to route_to('v1/invitations#upload')
    end
    # NOTE: below are commented out as they are not required so far.

    # it 'routes to #show' do
    #   expect(get: '/v1/invitations/1').to route_to('v1/invitations#show', id: '1')
    # end
    #
    # it 'routes to #create' do
    #   expect(post: '/v1/invitations').to route_to('v1/invitations#create')
    # end
    #
    # it 'routes to #update via PUT' do
    #   expect(put: '/v1/invitations/1').to route_to('v1/invitations#update', id: '1')
    # end
    #
    # it 'routes to #update via PATCH' do
    #   expect(patch: '/v1/invitations/1').to route_to('v1/invitations#update', id: '1')
    # end
    #
    # it 'routes to #destroy' do
    #   expect(delete: '/v1/invitations/1').to route_to('v1/invitations#destroy', id: '1')
    # end
  end
end
