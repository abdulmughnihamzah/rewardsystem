# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Customer, type: :model do
  describe 'customer attributes' do
    it 'has attributes id, name, points, created_at, and updated_at' do
      customer = build(:customer)
      expect(customer.attributes).to include('id', 'name', 'points', 'created_at', 'updated_at')
    end
  end

  describe 'customer validations' do
    context 'when name set to a taken name' do
      it 'is invalid' do
        create(:customer, name: 'X')
        customer1 = build(:customer, name: 'X')
        expect(customer1).not_to be_valid
      end
    end

    context 'when points set to negative points' do
      it 'is invalid' do
        customer = build(:customer, points: -1)
        expect(customer).not_to be_valid
      end
    end

    context 'when points set to positive decimal' do
      it 'accept it' do
        customer = create(:customer, points: 1.4)
        expect(customer.points).to eq(1.4)
      end
    end

    context 'when points set to zero' do
      it 'accept it' do
        customer = create(:customer, points: 0)
        expect(customer.points).to eq(0)
      end
    end
  end

  describe 'model associations' do
    context 'when associated to many sent invitations to different customers' do
      it 'is valid' do
        invited1 = create(:customer)
        invited2 = create(:customer)
        sent_invitations = create(:customer_with_sent_invitations, invited1: invited1, invited2: invited2).sent_invitations
        expect(sent_invitations.length).to eq(2)
      end
    end

    context 'when associated to many sent invitations to the same invited' do
      it 'is valid' do
        invited = create(:customer)
        sent_invitations = create(:customer_with_sent_invitations, invited1: invited, invited2: invited).sent_invitations
        expect(sent_invitations.length).to eq(2)
      end
    end

    context 'when associated to many received invitations from different inviters' do
      it 'is valid' do
        inviter1 = create(:customer)
        inviter2 = create(:customer)
        received_invitations = create(:customer_with_received_invitations, inviter1: inviter1, inviter2: inviter2).received_invitations
        expect(received_invitations.length).to eq(2)
      end
    end

    context 'when associated to many received invitations from the same inviter' do
      it 'is valid' do
        inviter = create(:customer)
        received_invitations = create(:customer_with_received_invitations, inviter1: inviter, inviter2: inviter).received_invitations
        expect(received_invitations.length).to eq(2)
      end
    end
  end
end
