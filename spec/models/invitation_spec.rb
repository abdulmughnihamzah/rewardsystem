# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Invitation, type: :model do
  describe 'invitations attributes' do
    it 'has attributes id, inviter_id, invited_id, invitation_date, invitation_time, accepted_at, created_at, updated_at' do
      invitation = build(:invitation)
      expect(invitation.attributes).to include('id', 'inviter_id', 'invited_id', 'invitation_date', 'invitation_time', 'accepted_at', 'created_at',
                                               'updated_at')
    end
  end

  describe 'invitations validations' do
    context 'when invitation datetime is in past' do
      it 'is valid' do
        invitation = build(:invitation, invitation_date: 1.day.ago.to_date, invitation_time: '01:01')
        expect(invitation).to be_valid
      end
    end

    context 'when invitation datetime is in future' do
      it 'is invalid' do
        invitation = build(:invitation, invitation_date: 1.day.from_now.to_date, invitation_time: '01:01')
        expect(invitation).not_to be_valid
      end
    end

    context 'when invitation_date is missing' do
      it 'is invalid' do
        invitation = build(:invitation, invitation_date: nil, invitation_time: '01:01')
        expect(invitation).not_to be_valid
      end
    end

    context 'when invitation_time is missing' do
      it 'is invalid' do
        invitation = build(:invitation, invitation_time: nil)
        expect(invitation).not_to be_valid
      end
    end

    context 'when inviter_id is missing' do
      it 'is invalid' do
        invitation = build(:invitation, inviter_id: nil)
        expect(invitation).not_to be_valid
      end
    end

    context 'when inviter_id is the same as invited_id' do
      it 'is invalid' do
        customer = build(:customer)
        invitation = build(:invitation, inviter_id: customer.id, invited_id: customer.id)
        expect(invitation).not_to be_valid
      end
    end

    context 'when invited_id is missing' do
      it 'is invalid' do
        invitation = build(:invitation, invited_id: nil)
        expect(invitation).not_to be_valid
      end
    end

    context 'when accepted_at is missing' do
      it 'is valid' do
        invitation = build(:invitation, accepted_at: nil)
        expect(invitation).to be_valid
      end
    end

    context 'when accepted_at is after invitation datetime' do
      it 'is valid' do
        invitation = build(:invitation, accepted_at: 1.day.ago, invitation_date: 2.days.ago)
        expect(invitation).to be_valid
      end
    end

    context 'when accepted_at is before invitation datetime' do
      it 'is invalid' do
        invitation = build(:invitation, accepted_at: 2.days.ago, invitation_date: 1.day.ago)
        expect(invitation).not_to be_valid
      end
    end

    context 'when accepted_at is in past' do
      it 'is valid' do
        invitation = build(:invitation, accepted_at: 2.days.ago, invitation_date: 3.days.ago)
        expect(invitation).to be_valid
      end
    end

    context 'when accepted_at is in future' do
      it 'is invalid' do
        invitation = build(:invitation, accepted_at: 1.minute.from_now, invitation_date: 3.days.ago)
        expect(invitation).not_to be_valid
      end
    end

    context 'when updating none empty accepted_at' do
      it 'is invalid' do
        invitation = create(:invitation, accepted_at: 3.days.ago, invitation_date: 3.days.ago)
        invitation.update(accepted_at: 1.day.ago)
        expect(invitation).not_to be_valid
      end
    end

    context 'when is not unique by inviter_id, invited_id, invitation_date, invitation_time' do
      it 'is invalid' do
        inviter = build(:customer)
        invited = build(:customer)
        create(:invitation, inviter: inviter, invited: invited, invitation_date: 3.days.ago, invitation_time: '01:01')
        invitation = build(:invitation, inviter: inviter, invited: invited, invitation_date: 3.days.ago, invitation_time: '01:01')
        expect(invitation).not_to be_valid
      end
    end
  end

  describe 'invitations associations' do
    it 'has inviter' do
      invitation = build(:invitation)
      expect(invitation.inviter).to be_instance_of(Customer)
    end

    it 'has invited' do
      invitation = build(:invitation)
      expect(invitation.invited).to be_instance_of(Customer)
    end
  end
end
