# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'v1/invitations', type: :request do
  let(:valid_attributes) do
    { invitations: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/invitations.txt')) }
  end

  let(:invalid_attributes) do
    {}
  end

  let(:invalid_invitations_format) do
    { invitations: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/invalid_invitations.txt')) }
  end

  let(:invalid_invitations_format2) do
    { invitations: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/invalid_invitations2.txt')) }
  end

  let(:valid_headers) do
    {}
  end

  describe 'POST /v1/upload' do
    context 'with valid parameters and contents' do
      it 'renders a JSON response with success' do
        post upload_v1_invitations_url, params: valid_attributes, headers: valid_headers
        expect(response).to have_http_status(:ok)
        expect(response.content_type).to match(a_string_including('application/json'))
      end

      it 'reward the descendant inviters based on level of their levels by 1/2^n where n is the level and starting at 0' do
        post upload_v1_invitations_url, params: valid_attributes, headers: valid_headers
        expect(JSON.parse(response.body)['data'].symbolize_keys).to include(A: '1.75', B: '1.5', C: '1.0')
      end
    end

    context 'with invalid parameters' do
      it 'renders a json response with errors' do
        post upload_v1_invitations_url, params: invalid_attributes
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with invalid invitations events log format' do
      it 'renders a json response with errors' do
        post upload_v1_invitations_url, params: invalid_invitations_format
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    context 'with invitations events log missing event name' do
      it 'renders a json response with errors' do
        post upload_v1_invitations_url, params: invalid_invitations_format2
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  #
  # describe 'GET /index' do
  #   it 'renders a successful response' do
  #     Invitation.create! valid_attributes
  #     get invitations_url, headers: valid_headers, as: :json
  #     expect(response).to be_successful
  #   end
  # end

  # describe 'GET /show' do
  #   it 'renders a successful response' do
  #     invitation = Invitation.create! valid_attributes
  #     get invitation_url(invitation), as: :json
  #     expect(response).to be_successful
  #   end
  # end

  # describe 'POST /create' do
  #   context 'with valid parameters' do
  #     it 'creates a new Invitation' do
  #       expect do
  #         post invitations_url,
  #              params: { invitation: valid_attributes }, headers: valid_headers, as: :json
  #       end.to change(Invitation, :count).by(1)
  #     end
  #
  #     it 'renders a JSON response with the new invitation' do
  #       post invitations_url,
  #            params: { invitation: valid_attributes }, headers: valid_headers, as: :json
  #       expect(response).to have_http_status(:created)
  #       expect(response.content_type).to match(a_string_including('application/json'))
  #     end
  #   end
  #
  #   context 'with invalid parameters' do
  #     it 'does not create a new Invitation' do
  #       expect do
  #         post invitations_url,
  #              params: { invitation: invalid_attributes }, as: :json
  #       end.to change(Invitation, :count).by(0)
  #     end
  #
  #     it 'renders a JSON response with errors for the new invitation' do
  #       post invitations_url,
  #            params: { invitation: invalid_attributes }, headers: valid_headers, as: :json
  #       expect(response).to have_http_status(:unprocessable_entity)
  #       expect(response.content_type).to match(a_string_including('application/json'))
  #     end
  #   end
  # end

  # describe 'PATCH /update' do
  #   context 'with valid parameters' do
  #     let(:new_attributes) do
  #       skip('Add a hash of attributes valid for your model')
  #     end
  #
  #     it 'updates the requested invitation' do
  #       invitation = Invitation.create! valid_attributes
  #       patch invitation_url(invitation),
  #             params: { invitation: new_attributes }, headers: valid_headers, as: :json
  #       invitation.reload
  #       skip('Add assertions for updated state')
  #     end
  #
  #     it 'renders a JSON response with the invitation' do
  #       invitation = Invitation.create! valid_attributes
  #       patch invitation_url(invitation),
  #             params: { invitation: new_attributes }, headers: valid_headers, as: :json
  #       expect(response).to have_http_status(:ok)
  #       expect(response.content_type).to match(a_string_including('application/json'))
  #     end
  #   end
  #
  #   context 'with invalid parameters' do
  #     it 'renders a JSON response with errors for the invitation' do
  #       invitation = Invitation.create! valid_attributes
  #       patch invitation_url(invitation),
  #             params: { invitation: invalid_attributes }, headers: valid_headers, as: :json
  #       expect(response).to have_http_status(:unprocessable_entity)
  #       expect(response.content_type).to match(a_string_including('application/json'))
  #     end
  #   end
  # end

  # describe 'DELETE /destroy' do
  #   it 'destroys the requested invitation' do
  #     invitation = Invitation.create! valid_attributes
  #     expect do
  #       delete invitation_url(invitation), headers: valid_headers, as: :json
  #     end.to change(Invitation, :count).by(-1)
  #   end
  # end
end
