# frozen_string_literal: true

FactoryBot.define do
  factory :invitation do
    sequence(:id, 1)
    invitation_date { 1.day.ago.to_date.to_s }
    sequence(:invitation_time, 1) { |t| Time.zone.at(1.day.ago.hour.hours + 1.day.ago.hour.minutes + t.minute).to_s(:time) }
    association :inviter, factory: :customer
    association :invited, factory: :customer
  end
end
