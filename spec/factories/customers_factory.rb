# frozen_string_literal: true

FactoryBot.define do
  factory :customer do
    sequence(:id, 1)
    sequence(:name, 'A')
    points { 0 }
    created_at { Time.current.to_date.to_s }
    updated_at { Time.current.to_s(:time) }

    # generate customer with associated sent_invitations
    factory :customer_with_sent_invitations do
      transient do
        invited1 { nil }
        invited2 { nil }
      end
      sent_invitations { [] }
      after(:create) do |customer, evaluator|
        create(:invitation, inviter: customer, invited: evaluator.invited1)
        create(:invitation, inviter: customer, invited: evaluator.invited2)
        customer.reload
      end
    end

    # generate customer with associated received_invitations
    factory :customer_with_received_invitations do
      transient do
        inviter1 { nil }
        inviter2 { nil }
      end
      received_invitations { [] }
      after(:create) do |customer, evaluator|
        create(:invitation, inviter: evaluator.inviter1, invited: customer)
        create(:invitation, inviter: evaluator.inviter2, invited: customer)
        customer.reload
      end
    end
  end
end
