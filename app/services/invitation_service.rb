# frozen_string_literal: true

# module Services
module InvitationService
  def self.create_and_reward(file)
    # Important Note: a customer (i.e the inviter or invited ) is supposed to be already created,
    # but as this is not the domain of this task; and we have not creating them before uploading
    # the file, I will create them if not any present.
    inviter_names = []
    line_number = 1
    File.open(file, 'r').each_line do |line|
      parts = line.strip.split(/\s+/)
      event_name = parts.find { |e| e.downcase.in?(%w[recommends accepts]) }&.downcase
      # check the format of the line. it should contain all and only the parts we interested in
      validate_format(parts, event_name, line, line_number)
      if event_name.eql?('recommends')
        date, time, inviter_name, _, invited_name = parts
        inviter = Customer.find_or_create_by!(name: inviter_name)
        invited = Customer.find_or_create_by!(name: invited_name)
        # In case the invitation record is new, then the accepted boolean value default
        # is false (set in level of database via migration).
        Invitation.find_or_create_by!(inviter_id: inviter.id, invited_id: invited.id, invitation_date: date, invitation_time: time)
        inviter_names << inviter.name
      else # event == accepts
        date, time, invited_name, = parts
        # Important Note:
        # We suppose that invitation always is logged before acceptance.
        # So, we just skip if no invitation has been recorded yet.
        # Another option is to raise exception and cancel the entire  file uploading transaction.
        # it depends on other criterion that is byeond this task
        invited = Customer.find_by(name: invited_name)
        next if invited.blank?

        invitation = invited.first_received_invitation
        next if invitation.blank?

        # Mark first received invitation as accepted through setting the  accepted_at datetime
        # This update will trigger the after save callback, which implementimg the rewarding algorithm for descendant inviters
        invitation.update!(accepted_at: Date.strptime(date, '%Y-%m-%d') + Time.zone.parse(time).seconds_since_midnight.seconds)

      end
      line_number += 1
    end
    inviter_names
  end

  ################################################################################
  # recursively reward the descendant inviters using division function
  def self.reward_inviter(inviter, level)
    reward = 1.0 / (2**level)
    inviter.update!(points: inviter.points + reward)
    up_inviter = inviter.first_received_invitation&.inviter
    reward_inviter(up_inviter, level + 1) if up_inviter.present?
  end

  ################################################################################
  def self.validate_format(parts, event_name, line, line_number)
    raise StandardError, 'Unknow event name' if event_name.blank?

    format_error_message = "Incorrect format, line #{line_number}, '#{line}'"
    raise StandardError, format_error_message unless event_name.present? && parts.size >= 4 && parts[3]&.downcase == event_name

    case event_name
    when 'recommends'
      raise StandardError, format_error_message unless parts.length == 5
    when 'accepts'
      raise StandardError, format_error_message unless parts.length == 4
    end
  end
end
