# frozen_string_literal: true

class Customer < ApplicationRecord
  # associations
  has_many :sent_invitations, class_name: 'Invitation', foreign_key: :inviter_id, dependent: :destroy,
                              inverse_of: :inviter
  has_many :invited, through: :sent_invitations

  has_many :received_invitations, class_name: 'Invitation', foreign_key: :invited_id, dependent: :destroy,
                                  inverse_of: :invited
  has_many :inviters, through: :sent_invitations
  has_one :first_received_invitation, lambda {
                                        order(:invitation_date, :invitiation_time)
                                      }, class_name: 'Invitation', foreign_key: :invited_id, dependent: :destroy,
                                         inverse_of: :invited

  # validation
  validates  :name, presence: true, uniqueness: true
  validates  :points, numericality: { greater_than_or_equal_to: 0 }
end
