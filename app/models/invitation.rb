# frozen_string_literal: true

class Invitation < ApplicationRecord
  # Associations

  belongs_to :inviter, class_name: 'Customer', inverse_of: :sent_invitations
  belongs_to :invited, class_name: 'Customer', inverse_of: :received_invitations

  # Validations

  validates :invitation_date, presence: true
  validates :invitation_time, presence: true
  # Each invitation  record should be uniqe on inviter_id, invited_id, invitation_date, invitation_time
  validates :inviter_id, uniqueness: { scope: %i[invited_id invitation_date invitation_time], message: 'duplicated invitation' }
  # inviter and invited cannot be the same customer
  validates :inviter_id, exclusion: { in: proc { |obj| [obj.invited_id] }, message: 'is not allowed to invite himself' }
  # accepted_at is not allowed to be updated for another time once set
  validate :accepted_at_updating, if: -> { changed.include?('accepted_at') && accepted_at_was.present? }
  # acceptance datetime must be in past When get recorded
  validate :accepted_at_period, if: -> { accepted_at.present? }
  # it does not accept invitation acceptance date (accepted_at) if is befor the invitation date+time
  validate :accepted_at_against_invitation_time, if: -> { accepted_at.present? }
  # it does not accept invitation whose invitation date+time is in future
  validate :invitation_datetime_period, if: -> { invitation_date.present? && invitation_time.present? }

  # Callbacks

  # reward inviter if invitation is the first accepted and for the first time. this callback does its own checks regardless of validations
  after_save :reward_inviter, if: lambda {
                                    saved_changes.include?('accepted_at') && saved_changes['accepted_at'][0].blank? &&
                                      invited.first_received_invitation == self
                                  }

  def invitation_datetime
    invitation_date.to_datetime + invitation_time.hour.hours + invitation_time.hour.minutes
  end

  private

  def reward_inviter
    InvitationService.reward_inviter(inviter, 0)
  end

  def accepted_at_updating
    errors.add(:accepted_at, 'is not allowed to be be updated unless was empty!')
  end

  def accepted_at_against_invitation_time
    errors.add(:accepted_at, 'is not valid to be before invitation datetime') if accepted_at < invitation_datetime
  end

  def accepted_at_period
    errors.add(:accepted_at, 'is not valid to be in future') if accepted_at > Time.current
  end

  def invitation_datetime_period
    errors.add(:base, 'invitation datetime is not valid to be in future') if invitation_datetime > Time.current
  end
end
