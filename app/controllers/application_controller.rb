# frozen_string_literal: true

# This controller is parent of all version controllers
# So, only logic shared among all versions is palced here
class ApplicationController < ActionController::API
  # Example of shared logic handling
  rescue_from Exception, with: :notify!

  private

  def notify!(exception)
    # Here we put the code to send notification through any communication channels, such as slack.
  end
end
