# frozen_string_literal: true

module V1
  class InvitationsController < BaseController
    # before_action :set_invitation, only: %i[show update destroy]

    def upload
      points = {}
      ActiveRecord::Base.transaction do
        inviter_names = InvitationService.create_and_reward(invitation_upload_params[:invitations])
        points = Customer.where(name: inviter_names).all.each_with_object({}) { |c, memo| memo[c.name] = c.points }
      end
      render json: { success: true, message: 'Points have been successfully credited to inviters', data: points }, status: :ok
      # error response is made to be handled by BaseController if StandardError, otherwise, by rails configured api response.
      # remember we use config.debug_exception_response_format = :api
    end

    # # GET /invitations
    # def index
    #   @invitations = Invitation.all
    #
    #   render json: @invitations
    # end
    #
    # # GET /invitations/1
    # def show
    #   render json: @invitation
    # end
    #
    # # POST /invitations
    # def create
    #   @invitation = Invitation.new(invitation_params)
    #
    #   if @invitation.save
    #     render json: @invitation, status: :created, location: @invitation
    #   else
    #     render json: @invitation.errors, status: :unprocessable_entity
    #   end
    # end
    #
    # # PATCH/PUT /invitations/1
    # def update
    #   if @invitation.update(invitation_params)
    #     render json: @invitation
    #   else
    #     render json: @invitation.errors, status: :unprocessable_entity
    #   end
    # end
    #
    # # DELETE /invitations/1
    # def destroy
    #   @invitation.destroy
    # end

    private

    # # Use callbacks to share common setup or constraints between actions.
    # def set_invitation
    #   @invitation = Invitation.find(params[:id])
    # end
    #
    # # Only allow a list of trusted parameters through.
    # def invitation_params
    #   params.require(:invitation).permit(:inviter_id, :invited_id, :invitation_date,
    #                                      :invitation_time, :accepted)
    # end

    def invitation_upload_params
      params.permit(:invitations)
    end
  end
end
