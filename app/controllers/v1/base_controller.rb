# frozen_string_literal: true

# Offer the parent calss for the sane version children controllers.
# Shared logic and filters must be placed here, in the base controller
module V1
  class BaseController < ApplicationController
    # made custome response on errors
    rescue_from StandardError, with: :handle_error_response

    private

    def handle_error_response(exception)
      # Render custom response. it is better to follow  the same pattern of response that is helpful for client applications
      render json: { success: false, message: 'Internal Server Error', error: exception.message }, status: :unprocessable_entity
    end
  end
end
