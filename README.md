# Introduction

Through this code challenge, I tried to present my rails skills and follow my own habit of coding and designing solutions. So, I did my best to make it easier for the reviewer to make her/his decision with high confidence.

Therefore, I tried not to make the service as simple as to just respond with the correct result and apply its test; Rather, I developed an extendable, scalable, reliable solution with an efficient, reusable,  and clean code.

To some extent, it may not be the best of its kind, as there's always a better way to do something.  However, it demonstrates two things:

1. My RoR skills
2. My passion and enthusiasm to join CLARK
---
# Solution Summary
The solution was implemented as pure RoR API, and has two associated models invitations and customers each has its associations and validations. A customer can play the role of inviter and/or invited and an inviter and invited are joined through the invitations model. Customers are unique and only added if they are new.

The invitation model record joins an inviter to an invited of the same class but not the same instance. Also, an invitation record keeps track of the invitation date & time, the acceptance date, and the creation and updating of timestamps. The importance of saving invitations in database with customers as normalized relations is:

- 	It can be used for further scaling of the solution and generating reports, and charts, for example. Studying the relationships, reporting the best performing customers, the average of invitations a customer made. etc. 
- 	It enables to discover the duplication of posted invitations through different requests. The duplicated invitations are when having the same inviter, invited, and the same issuing time. This means we can guarantee to not reward a customer for the same invitation instance multiple time as we have the history.

An acceptance of the invitation is ensured to only be applicable to the first received invitation and ignored for others. This requirement is achieved via validations and checks. The invitations are accepted to be saved to database only if they are unique by the inviter, invited and its date & time. Otherwise, it is only replicated logs. An ‘accept’ event effects on the first related invitation and set its acceptance date using column ‘accepted_at’, which functioning as a flag to mark the accepted invitation and keep track of acceptance time as well.  Another approach is to use events model and keep log of two types of events recommendation and acceptance. However, I preferred using invitation model, as it can keep the two types of events more efficiently. It explicitly keeps track of recommendation, and, at the same time, it keeps track of acceptance events implicitly. This achieves the requirements in lower number of records and more correlated data.  An inviter gets rewarded when having its invitation accepted. this was implemented via utilizing rails ActiveRecords callbacks. And when an inviter gets rewarded all their descendants are get rewarded according to dividing exponential function (1/2^n) via recursively traversing the path up until the root inviter. The associations and validations are utilized to address the solution conditions and concerns effectively.
The testing part (TDD) was implemented thoroughly using RSpec framework and the. Factory bot (Factory girl). And all the involved application units, such as models, requests, and routes are covered by the test 100% using the SimpleCov tool report.

---
# What I have Did
1. Implemented invitation & customer models, their controllers, and associations & validations as raw logs history records. 
2. Implemented uploading invitations log event file service to create events users, and reward inviters according to the requested function.
3. Commented out the CRUD operations, routes, and testing that are not required by this code challenge for the reviewer to see only the parts he wants.
4. Versioning API (namespacing) and put controllers under V1.
5. Used custom response format for success or error result for a request and made the base controller to handle error response.
6. Implemented testing using RSpec
	..* Tested models attributes, associations, and validations.
	..* Tested routes.
	..* Tested Requests (uploading file of invitations).
7. Used SimpleCov to analyze and report testing coverage
8. Added Rubocop config.
---
# Instructions
## To Run Service
To run the service as you know, we go to the project directory and do `rails s`

*The uploading service URL is*:

`http://localhost:3000/v1/invitations/upload`

Either use Curl or Postman

### In case of using Curl the request command is:

`Curl -F invitations=@/path/to/file.txt http://localhost:3000/v1/invitations/upload` 

as shown in this image below

![Screen Shot 2022-04-09 at 7.24.17 AM.png](https://bitbucket.org/repo/dx5G5aq/images/1038379261-Screen%20Shot%202022-04-09%20at%207.24.17%20AM.png)

### In case of using Postman just:
+ Select POST method.
+ Insert the service URL.
+ And select body as form-data.
+ Set the param key ‘invitations’ and select the log file as shown in the below image.

![Screen Shot 2022-04-09 at 7.23.38 AM.png](https://bitbucket.org/repo/dx5G5aq/images/3207112692-Screen%20Shot%202022-04-09%20at%207.23.38%20AM.png)

## To Run Test
Go to the project root directory and do `rspec`

This will run test and will generate the test coverage analysis. you can find the report under coverage directory. Below is a snapshot of it.

![Screen Shot 2022-04-09 at 7.22.35 AM.png](https://bitbucket.org/repo/dx5G5aq/images/30540998-Screen%20Shot%202022-04-09%20at%207.22.35%20AM.png)