# frozen_string_literal: true

Rails.application.routes.draw do
  # Versioning of API as project requirements are prone to changes.
  # vergining ensure we savely serve the clients that use the old versions of the backend application
  namespace :v1 do
    # resources :customers
    resources :invitations, only: [] do
      collection do
        post :upload
      end
    end
  end
end
